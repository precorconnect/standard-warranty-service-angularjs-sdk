(function () {
    angular.module(
        "standardWarrantyServiceModule",
        []);
})();
(function () {
    angular
        .module("standardWarrantyServiceModule")
        .provider(
        'standardWarrantyServiceConfig',
        standardWarrantyServiceConfigProvider
    );

    function standardWarrantyServiceConfigProvider() {

        var objectUnderConstruction = {
            setBaseUrl: setBaseUrl,
            $get: $get
        };

        return objectUnderConstruction;

        function setBaseUrl(baseUrl) {
            objectUnderConstruction.baseUrl = baseUrl;
            return objectUnderConstruction;
        }

        function $get() {
            return {
                baseUrl: objectUnderConstruction.baseUrl
            }
        }
    }
})();
(function () {
    angular
        .module('standardWarrantyServiceModule')
        .factory(
        'standardWarrantyServiceClient',
        [
            'standardWarrantyServiceConfig',
            '$http',
            '$q',
            standardWarrantyServiceClient
        ]);

    function standardWarrantyServiceClient(standardWarrantyServiceConfig,
                                           $http,
                                           $q) {

        var lastSubmittedCommercialCustomerPurchaseRegistration,
            lastSubmittedConsumerCustomerPurchaseRegistration,
            lastSubmittedPartnerInstallRegistration;

        return {
            constructCommercialCustomerPurchaseRegistration: constructCommercialCustomerPurchaseRegistration,
            constructConsumerCustomerPurchaseRegistration: constructConsumerCustomerPurchaseRegistration,
            constructPartnerInstallRegistration: constructPartnerInstallRegistration,
            constructProductPurchaseInformation: constructProductPurchaseInformation,
            constructCompositeProductSaleInformation: constructCompositeProductSaleInformation,
            constructSimpleProductSaleInformation: constructSimpleProductSaleInformation,
            getCommercialCustomerPurchaseRegistration: getCommercialCustomerPurchaseRegistration,
            getConsumerCustomerPurchaseRegistration: getConsumerCustomerPurchaseRegistration,
            getPartnerInstallRegistration: getPartnerInstallRegistration,
            submitCommercialCustomerPurchaseRegistration: submitCommercialCustomerPurchaseRegistration,
            submitConsumerCustomerPurchaseRegistration: submitConsumerCustomerPurchaseRegistration,
            submitPartnerInstallRegistration: submitPartnerInstallRegistration
        };

        /**
         * Submission information
         * @typedef {Object} SubmissionInformation
         * @property {number} timestamp
         * @property {string} id
         */

        /**
         * A mailing address
         * @typedef {Object} Address
         * @property {string} line1
         * @property {string} [line2]
         * @property {string} city
         * @property {string} region
         * @property {string} postalCode
         * @property {string} countryCode - iso 3166-1-alpha-2 country code
         */

        /**
         * Commercial purchase registration facility information
         * @typedef {Object} CommercialCustomerPurchaseRegistration_FacilityInformation
         * @property {string} name
         * @property {Address} address
         * @property {FacilityContactInformation} contact
         */

        /**
         * Information about a contact for a facility
         * @typedef {Object} FacilityContactInformation
         * @property {string} firstName
         * @property {string} lastName
         * @property {string} phoneNumber
         * @property {string} emailAddress
         */

        /**
         * Commercial purchase registration  purchase information
         * @typedef {Object} CommercialCustomerPurchaseRegistration_PurchaseInformation
         * @property {string} date
         * @property {ProductPurchaseInformation[]} products
         */

        /**
         * A commercial customer purchase registration
         * @typedef {Object} CommercialCustomerPurchaseRegistration
         * @property {CommercialCustomerPurchaseRegistration_FacilityInformation} facilityInformation
         * @property {CommercialCustomerPurchaseRegistration_PurchaseInformation} purchaseInformation
         * @property {SubmissionInformation} submissionInformation
         */

        /**
         * Constructs a CommercialCustomerPurchaseRegistration
         * @returns {CommercialCustomerPurchaseRegistration}
         */
        function constructCommercialCustomerPurchaseRegistration() {
            return {
                facilityInformation: {
                    name: null,
                    address: {
                        line1: null,
                        line2: null,
                        city: null,
                        region: null,
                        postalCode: null,
                        countryCode: null
                    },
                    contact: {
                        firstName: null,
                        lastName: null,
                        phoneNumber: null,
                        emailAddress: null
                    }
                },
                purchaseInformation: {
                    date: null,
                    products: []
                },
                submissionInformation: {
                    id: null,
                    timestamp: null
                }
            };
        }

        /**
         * Consumer purchase registration customer information
         * @typedef {Object} ConsumerCustomerPurchaseRegistration_CustomerInformation
         * @property {string} firstName
         * @property {string} lastName
         * @property {Address} address
         * @property {string} phoneNumber
         * @property {string} emailAddress
         */

        /**
         * Consumer purchase registration purchase information
         * @typedef {Object} ConsumerCustomerPurchaseRegistration_PurchaseInformation
         * @property {string} date
         * @property {ProductPurchaseInformation[]} products
         */

        /**
         * A consumer purchase registration
         * @typedef {Object} ConsumerCustomerPurchaseRegistration
         * @property {ConsumerCustomerPurchaseRegistration_CustomerInformation} customerInformation
         * @property {ConsumerCustomerPurchaseRegistration_PurchaseInformation} purchaseInformation
         * @property {SubmissionInformation} submissionInformation
         */

        /**
         * Constructs a ConsumerCustomerPurchaseRegistration
         * @returns {ConsumerCustomerPurchaseRegistration}
         */
        function constructConsumerCustomerPurchaseRegistration() {
            return {
                customerInformation: {
                    firstName: null,
                    lastName: null,
                    address: {
                        line1: null,
                        line2: null,
                        city: null,
                        region: null,
                        postalCode: null,
                        countryCode: null
                    },
                    phoneNumber: null,
                    emailAddress: null
                },
                purchaseInformation: {
                    date: null,
                    products: []
                },
                submissionInformation: {
                    id: null,
                    timestamp: null
                }
            };
        }

        /**
         * Partner install registration installation information
         * @typedef {Object} PartnerInstallRegistration_InstallationInformation
         * @property: {string} date
         * @property {CompositeProductSaleInformation[]} products
         */

        /**
         * Partner install registration
         * @typedef {Object} PartnerInstallRegistration
         * @property {string} sapOrderNumber
         * @property {PartnerInstallRegistration_InstallationInformation} installInformation
         * @property {SubmissionInformation} submissionInformation
         */

        /**
         * Constructs a PartnerInstallRegistration
         * @returns {PartnerInstallRegistration}
         */
        function constructPartnerInstallRegistration() {
            return {
                sapOrderNumber: null,
                installInformation: {
                    date: null,
                    products: []
                },
                submissionInformation: {
                    id: null,
                    timestamp: null
                }
            }
        }

        /**
         * Information about a single purchased product
         * @typedef {Object} ProductPurchaseInformation
         * @property {string} serialNumber
         * @property {string} description
         */

        /**
         * Constructs a ProductPurchaseInformation
         * @returns {ProductPurchaseInformation}
         */
        function constructProductPurchaseInformation() {
            return {
                serialNumber: null,
                description: null
            };
        }

        /**
         * Information about a single composite product sale
         * @typedef {Object} CompositeProductSaleInformation
         * @property {string} [price] - (only populated if sold with unit pricing)
         * @property {SimpleProductSaleInformation[]} [components]
         */

        /**
         * Constructs a CompositeProductSaleInformation
         * @returns {CompositeProductSaleInformation}
         */
        function constructCompositeProductSaleInformation() {
            return {
                price: null,
                components: []
            };
        }

        /**
         * Information about a single simple product sale
         * @typedef {Object} SimpleProductSaleInformation
         * @property {string} productGroup
         * @property {string} productLine
         * @property {string} serialNumber
         * @property {string} description
         * @property {string} price
         */

        /**
         * Constructs a SimpleProductSaleInformation
         * @returns {SimpleProductSaleInformation}
         */
        function constructSimpleProductSaleInformation() {
            return {
                productGroup: null,
                productLine: null,
                serialNumber: null,
                description: null,
                price: null
            };
        }

        /**
         * Gets a commercial purchase registration
         * @param submissionId
         * @returns {CommercialCustomerPurchaseRegistration}
         */
        function getCommercialCustomerPurchaseRegistration(submissionId) {
            // use local data if we have it
            if (lastSubmittedCommercialCustomerPurchaseRegistration
                && lastSubmittedCommercialCustomerPurchaseRegistration.submissionInformation
                && lastSubmittedCommercialCustomerPurchaseRegistration.submissionInformation.id == submissionId) {
                var deferred = $q.defer();
                deferred.resolve(lastSubmittedCommercialCustomerPurchaseRegistration);
                return deferred.promise;
            }
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve(
                    {
                        facilityInformation: {
                            name: "Test facility",
                            address: {
                                line1: "222 snowflake lane",
                                line2: null,
                                city: "north pole",
                                region: "north region",
                                postalCode: "234233",
                                countryCode: "US"
                            },
                            contact: {
                                firstName: "Kris",
                                lastName: "Kringle",
                                phoneNumber: "8675309",
                                emailAddress: "kris@christmas.com"
                            }
                        },
                        purchaseInformation: {
                            date: "02/03/2014",
                            products: [
                                {
                                    serialNumber: "AD23423333",
                                    description: "Strength - Commercial"
                                }
                            ]
                        },
                        submissionInformation: {
                            id: "23423423",
                            timestamp: 234234234
                        }
                    });
                return deferred.promise;

            }

            /*
             var request = $http({
             method: "get",
             url: standardWarrantyServiceConfig.baseUrl + "/commercial-purchase-registrations",
             params: {
             submissionId: submissionId
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Gets a consumer purchase registration
         * @param submissionId
         * @returns {ConsumerCustomerPurchaseRegistration}
         */
        function getConsumerCustomerPurchaseRegistration(submissionId) {

            // use local data if we have it
            if (lastSubmittedConsumerCustomerPurchaseRegistration
                && lastSubmittedConsumerCustomerPurchaseRegistration.submissionInformation
                && lastSubmittedConsumerCustomerPurchaseRegistration.submissionInformation.id == submissionId) {
                var deferred = $q.defer();
                deferred.resolve(lastSubmittedConsumerCustomerPurchaseRegistration);
                return deferred.promise;
            }
            // otherwise get it from the server
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve({
                    customerInformation: {
                        firstName: "testy",
                        lastName: "mctesterson",
                        address: {
                            line1: "1000 snowflake lane",
                            line2: null,
                            city: "north pole",
                            region: "north region",
                            postalCode: "239393",
                            countryCode: "US"
                        },
                        phoneNumber: "222-222-2222",
                        emailAddress: "test@test.com"
                    },
                    purchaseInformation: {
                        date: "02/02/2012",
                        products: [
                            {
                                serialNumber: "AD23423333",
                                description: "Strength - Consumer"
                            }
                        ]
                    },
                    submissionInformation: {
                        id: submissionId,
                        timestamp: 234234234
                    }
                });
                return deferred.promise;
            }

            /*
             var request = $http({
             method: "get",
             url: standardWarrantyServiceConfig.baseUrl + "/consumer-purchase-registrations",
             params: {
             submissionId: submissionId
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Gets a partner install registration by its submission id
         * @param submissionId
         * @returns {PartnerInstallRegistration}
         */
        function getPartnerInstallRegistration(submissionId) {
            // use local data if we have it
            if (lastSubmittedConsumerCustomerPurchaseRegistration
                && lastSubmittedConsumerCustomerPurchaseRegistration.submissionInformation
                && lastSubmittedConsumerCustomerPurchaseRegistration.submissionInformation.id == submissionId) {
                var deferred = $q.defer();
                deferred.resolve(lastSubmittedConsumerCustomerPurchaseRegistration);
                return deferred.promise;
            }
            // otherwise get it from the server
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve(
                    {
                        sapOrderNumber: "234333323332",
                        installInformation: {
                            date: "02/02/2012",
                            products: [
                                {
                                    serialNumber: "AD23423333",
                                    productGroup: "Strength Multi Station Icarian",
                                    productLine: "Strength - Commercial"
                                },
                                {
                                    components: [
                                        {
                                            serialNumber: "A925D17130D01",
                                            productGroup: "Treadmill - Commercial",
                                            productLine: "Treadmill Base - Commercial"
                                        },
                                        {
                                            serialNumber: "BD633A00F0DD",
                                            productGroup: "EFX - Commercial",
                                            productLine: "EFX 537"
                                        }
                                    ]
                                }
                            ]
                        },
                        submissionInformation: {
                            id: submissionId,
                            timestamp: 234234234
                        }
                    });
                return deferred.promise;
            }

            /*
             var request = $http({
             method: "get",
             url: standardWarrantyServiceConfig.baseUrl + "/partner-install-registrations",
             params: {
             submissionId: submissionId
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Submits a commercial purchase registration
         * @param {CommercialCustomerPurchaseRegistration} commercialCustomerPurchaseRegistration
         * @returns {string} a submission id
         */
        function submitCommercialCustomerPurchaseRegistration(commercialCustomerPurchaseRegistration) {
            // use dummy data for now
            var deferred = $q.defer();
            commercialCustomerPurchaseRegistration.submissionInformation.id = new Date().getTime();
            commercialCustomerPurchaseRegistration.submissionInformation.timestamp = Math.floor(new Date().getTime() / 1000);
            lastSubmittedCommercialCustomerPurchaseRegistration = commercialCustomerPurchaseRegistration;
            deferred.resolve((new Date().getTime()));
            return deferred.promise;

            /*var request = $http({
             method: "post",
             url: standardWarrantyServiceConfig.baseUrl + "/commercial-customer-purchase-registrations",
             data: commercialCustomerPurchaseRegistration
             });

             return request
             .then
             (
             handleSuccess,
             handleError
             )
             .then
             (
             function (id) {
             // set submission id and timestamp and store for confirmation page
             commercialCustomerPurchaseRegistration.submissionInformation.id = id;

             commercialCustomerPurchaseRegistration.submissionInformation.timestamp =
             Math.floor(new Date().getTime()/1000);

             lastSubmittedCommercialCustomerPurchaseRegistration = commercialCustomerPurchaseRegistration;
             return id;
             },
             function (reason) {
             return $q.reject(reason);
             }
             );*/
        }

        /**
         * Submits a consumer purchase registration
         * @param {ConsumerCustomerPurchaseRegistration} consumerCustomerPurchaseRegistration
         * @returns {string} a submission id
         */
        function submitConsumerCustomerPurchaseRegistration(consumerCustomerPurchaseRegistration) {
            // use dummy data for now
            var deferred = $q.defer();
            consumerCustomerPurchaseRegistration.submissionInformation.id = new Date().getTime();
            consumerCustomerPurchaseRegistration.submissionInformation.timestamp = Math.floor(new Date().getTime() / 1000);
            lastSubmittedConsumerCustomerPurchaseRegistration = consumerCustomerPurchaseRegistration;
            deferred.resolve((new Date().getTime()));
            return deferred.promise;

            /*var request = $http({
             method: "post",
             url: standardWarrantyServiceConfig.baseUrl + "/consumer-customer-purchase-registrations",
             data: consumerCustomerPurchaseRegistration
             });

             return request
             .then
             (
             handleSuccess,
             handleError
             )
             .then
             (
             function (id) {
             // set submission id and timestamp and store for confirmation page
             consumerCustomerPurchaseRegistration.submissionInformation.id = id;

             consumerCustomerPurchaseRegistration.submissionInformation.timestamp =
             Math.floor(new Date().getTime()/1000);

             lastSubmittedConsumerCustomerPurchaseRegistration = consumerCustomerPurchaseRegistration;
             return id;
             },
             function (reason) {
             return $q.reject(reason);
             }
             );*/
        }

        /**
         * Submits a partner install registration
         * @param {PartnerInstallRegistration} partnerInstallRegistration
         * @returns {string} a submission id
         */
        function submitPartnerInstallRegistration(partnerInstallRegistration) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerInstallRegistration.submissionInformation.id = new Date().getTime();
            partnerInstallRegistration.submissionInformation.timestamp = Math.floor(new Date().getTime() / 1000);
            lastSubmittedPartnerInstallRegistration = partnerInstallRegistration;
            deferred.resolve((new Date().getTime()));
            return deferred.promise;

            /*var request = $http({
             method: "post",
             url: standardWarrantyServiceConfig.baseUrl + "/partner-install-registrations",
             data: partnerInstallRegistration
             });

             return request
             .then
             (
             handleSuccess,
             handleError
             )
             .then
             (
             function (id) {
             // set submission id and timestamp and store for confirmation page
             partnerInstallRegistration.submissionInformation.id = id;

             partnerInstallRegistration.submissionInformation.timestamp =
             Math.floor(new Date().getTime()/1000);

             lastSubmittedPartnerInstallRegistration = partnerInstallRegistration;
             return id;
             },
             function (reason) {
             return $q.reject(reason);
             }
             );*/
        }

        function handleError(response) {
            if (
                !angular.isObject(response.data) || !response.data.message
            ) {

                return ( $q.reject("An unknown error occurred.") );

            }

            // Otherwise, use expected error message.
            return ( $q.reject(response.data.message) );

        }

        function handleSuccess(response) {

            return ( response.data );

        }
    }
})();
