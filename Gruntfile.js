module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.initConfig({
        concat:{
            dist: {
                src: ['src/standard-warranty-service.module.js','src/*.js'],
                dest: 'dist/standard-warranty-service-angularjs-sdk.js'
            }
        }
    });
    grunt.registerTask('default', ['concat']);
};