## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect standard warranty service.

## UseCases

#####constructCommercialCustomerPurchaseRegistration
Constructs a CommercialCustomerPurchaseRegistration

#####constructConsumerCustomerPurchaseRegistration
Constructs a ConsumerCustomerPurchaseRegistration

#####constructPartnerInstallRegistration
Constructs a PartnerInstallRegistration

#####constructProductPurchaseInformation
Constructs ProductPurchaseInformation (information about a single purchased product)

#####constructCompositeProductSaleInformation
Constructs CompositeProductSaleInformation (information about a single composite product sale)

#####constructSimpleProductSaleInformation
Constructs SimpleProductSaleInformation (information about a single simple product sale)

#####getCommercialCustomerPurchaseRegistration
Gets a commercial purchase registration by its submission id

#####getConsumerCustomerPurchaseRegistration
Gets a consumer purchase registration by its submission id

#####getPartnerInstallRegistration
Gets a partner install registration by its submission id

#####submitCommercialCustomerPurchaseRegistration
Submits a commercial purchase registration

#####submitConsumerCustomerPurchaseRegistration
Submits a consumer purchase registration

#####submitPartnerInstallRegistration
Submits a partner install registration

## Installation
add as bower dependency

```shell
bower install https://bitbucket.org/precorconnect/standard-warranty-service-angularjs-sdk.git --save
```
include in view
```html
<script src="bower-components/angular/angular.js"></script>
<script src="bower-components/standard-warranty-service-angularjs-sdk/dist/standard-warranty-service-angularjs-sdk.js"></script>
```
configure
see below.

## Configuration
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the standard warranty service. |

#### Example
```js
angular.module(
        "app",
        ["standardWarrantyServiceModule"])
        .config(
        [
            "standardWarrantyServiceConfigProvider",
            appConfig
        ]);

    function appConfig(standardWarrantyServiceConfigProvider) {
        standardWarrantyServiceConfigProvider
            .setBaseUrl("@@standardWarrantyServiceBaseUrl");
    }
```